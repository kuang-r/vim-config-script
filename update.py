#!/usr/bin/env python3

import os
import stat
import shutil
from pathlib import Path

# Check environment and get home path.
homepath = os.getenv("HOME")
if homepath:
    print("The environment is unix.")
    iswin = False
else:
    print("The environment is win.")
    iswin = True
    homepath = Path(os.path.expanduser("~"))
homepath = Path(homepath)

# Get the relevant address.
vimpath = homepath / "vimfiles" if iswin else homepath / ".vim"
vimpath.mkdir(0o744, exist_ok=True)
plugpath = vimpath / "plugin"

# Set vim config filename.
filename = "_vimrc" if iswin else ".vimrc"
configfile = homepath / filename

# Remove exist plugin.
if configfile.exists():
    configfile.unlink()
def remove_readonly(func, path, _):
    os.chmod(path, stat.S_IWRITE)
    func(path)
if plugpath.exists():
    print("Remove old plugin.")
    shutil.rmtree(plugpath, onerror=remove_readonly)

# Copy plugin.
print("Copy config file and plugin.")
shutil.copy(Path("vimrc.txt"), configfile)
shutil.copytree(Path("plugin"), vimpath / "plugin")
